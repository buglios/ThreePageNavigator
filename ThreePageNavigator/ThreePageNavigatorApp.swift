//
//  ThreePageNavigatorApp.swift
//  ThreePageNavigator
//
//  Created by Field Employee on 4/18/23.
//

import SwiftUI

@main
struct ThreePageNavigatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
